<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<title>HealthInsurance</title>
</head>
<body>

<form name="form" onsubmit="return validate(this)" action="gethealthquote" >

   <c:if test="${not empty message}"> 
      QuoteValue : ${message}
   </c:if>
  <br/>
  <br/>
  Name : <input type="text" name="name" id="name" size="30"/>
  <br/>
  <br/>
  Gender : <input type="radio" id="gender" name="gender" value="male" checked> Male <input type="radio" id="gender" name="gender" value="female">Female <input type="radio" id="gender" name="gender" value="other">Other
           <br>
           <br/>
  Age :    <input type="text" id="age" name="age" onkeypress='return event.charCode >= 48 && event.charCode <= 57' size="3"/>
  <br/>
 
  <br/>
  <div id="checkbox-value">
      <label for="hypertension">Current Health : </label>
      <br/>
	  <input type="checkbox" name="hypertension" id="hypertension"/>HyperTension<br/>
	  <input type="checkbox" name="bloodpressure" />Blood Pressure<br/>
	  <input type="checkbox" name="bloodsugar" />Blood Sugar<br/>
	  <input type="checkbox" name="overweight"/>Overweight<br/>
	  <br/>
	  <label for="smoking">Habits : </label>
	  <br/>
	  <input type="checkbox" name="smoking"/>Smoking<br/>
	  <input type="checkbox" name="alcohol" />Alcohal<br/>
	  <input type="checkbox" name="dailyexercise" />DailyExercise<br/>
	  <input type="checkbox" name="drugs"/>Drugs<br/>
  </div>
  <br/>
  <input type="submit" id="submit" value="Submit"/>
 
  <script type="text/javascript">
         
        if($("#hypertension").is(':checked')){
            $("#hypertension").attr('value', 'true');
        }else {
        	 $("#hypertension").attr('value', 'false');
        }
        
        if($("#bloodpressure").is(':checked')){
            $("#bloodpressure").attr('value', 'true');
        }else {
        	 $("#bloodpressure").attr('value', 'false');
        }
        
        if($("#bloodsugar").is(':checked')){
            $("#bloodsugar").attr('value', 'true');
        }else {
        	 $("#bloodsugar").attr('value', 'false');
        }
        
        if($("#overweight").is(':checked')){
            $("#overweight").attr('value', 'true');
        }else {
        	 $("#overweight").attr('value', 'false');
        }
        
        if($("#smoking").is(':checked')){
            $("#smoking").attr('value', 'true');
        }else {
        	 $("#smoking").attr('value', 'false');
        }
        if($("#drugs").is(':checked')){
            $("#drugs").attr('value', 'true');
        }else {
        	 $("#drugs").attr('value', 'false');
        }
        if($("#dailyexercise").is(':checked')){
            $("#dailyexercise").attr('value', 'true');
        }else {
        	 $("#dailyexercise").attr('value', 'false');
        }
        if($("#alcohol").is(':checked')){
            $("#alcohol").attr('value', 'true');
        }else {
        	 $("#alcohol").attr('value', 'false');
        }
    
         function validate(form) {
        	 var name= document.form.name.value.trim();
    	    if ( name == "") {
    	        alert("Enter a name");
    	        document.form.name.focus();
    	        return false;
    	    }
    	    if (!/^[a-zA-Z\s]*$/g.test(name)) {
    	        alert("Invalid characters");
    	        document.form.name.focus();
    	        return false;
    	    }
    	    if (document.form.age.value >100) {
    	        alert("Enter Correct age");
    	        document.form.age.focus();
    	        return false;
    	    }
    	}
</script>
  
</form>

</body>
</html>
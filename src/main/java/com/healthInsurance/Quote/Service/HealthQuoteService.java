package com.healthInsurance.Quote.Service;

public interface HealthQuoteService {

	public double getHealthQuote(String name, String gender, int age, boolean hyperTension, boolean bloodpressure,boolean bloodsugar,
			boolean overweight, boolean smoking, boolean alcohol, boolean dailyexercise, boolean drugs);
	
}

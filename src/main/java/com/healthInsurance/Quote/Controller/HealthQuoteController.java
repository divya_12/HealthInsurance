package com.healthInsurance.Quote.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.healthInsurance.Quote.Service.HealthQuoteService;

@Controller
public class HealthQuoteController {

	@Autowired 
	HealthQuoteService quoteService;
	
	
    @RequestMapping(value="/")
	   public ModelAndView index(HttpServletRequest req,HttpServletResponse response){
	     return new ModelAndView("home");  
	  }
	
	
	@RequestMapping(value="/gethealthquote")
	public ModelAndView getHealthQuote(HttpServletRequest request, HttpServletResponse response){
		
		String name=request.getParameter("name");
		String lastName="";
		if(name.trim().contains(" ")){
			String[] fullName= name.trim().split(" ");
			lastName=fullName[1];
		}
		String usedName= lastName!="" ? lastName : name;
		String gender = request.getParameter("gender");
		String title= gender.equals("male") ? "Mr." : "Miss/Mrs";
		int age= Integer.parseInt(request.getParameter("age"));
		boolean hyperTension= request.getParameter("hypertension") != null;
		boolean bloodPressure= request.getParameter("bloodpressure") != null;
		boolean bloodSugar= request.getParameter("bloodsugar") != null;
		boolean overweight= request.getParameter("overweight") != null;
		boolean smoking= request.getParameter("smoking") != null;
		boolean alcohol= request.getParameter("alcohol") != null;
		boolean dailyexercise= request.getParameter("dailyexercise") != null;
		boolean drugs= request.getParameter("drugs") != null;
	
		double quotevalue=quoteService.getHealthQuote(name,gender,age,hyperTension,bloodPressure,bloodSugar,overweight,smoking,alcohol,dailyexercise,drugs);
		return new ModelAndView("quoteCalculationForm").addObject("message", "Health Insurance Premium for "+ title+" "+ usedName +": Rs." + quotevalue);
	}
	
	
}

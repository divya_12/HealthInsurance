package com.healthInsurance.Quote.ServiceImpl;

import org.springframework.stereotype.Service;

import com.healthInsurance.Quote.Service.HealthQuoteService;

@Service
public class HealthQuoteServiceImpl implements HealthQuoteService {

	@Override
	public double getHealthQuote(String name, String gender, int age,
			boolean hyperTension, boolean bloodpressure, boolean bloodsugar,
			boolean overweight, boolean smoking, boolean alcohol,
			boolean dailyexercise, boolean drugs) {
		// TODO Auto-generated method stub
		
		double basePremium= 5000;
		double ageValue = age / 5.0;

		if (ageValue < 8) {
			if (ageValue < 5) {
				ageValue = 1;
			} else {
				ageValue = ageValue - 4;
				if (ageValue > 0.0) {
					ageValue=ageValue+1;
				}
			}

			basePremium = basePremium + basePremium * (int) ageValue * 0.10;
		} else {
			basePremium = basePremium + basePremium * 3 * 0.10;
			ageValue = ageValue - 8;
			if (ageValue > 0.0) {
				ageValue=ageValue+1;
			}
			basePremium = basePremium + basePremium * (int) ageValue * 0.20;
		}

		if(gender.equalsIgnoreCase("male")){
			basePremium += 0.02 * basePremium;
		}
		if(hyperTension){
			basePremium += 0.01 * basePremium;
		}
		if(bloodpressure){
			basePremium += 0.01 * basePremium;
		}
		if(bloodsugar){
			basePremium += 0.01 * basePremium;
		}
		if(overweight){
			basePremium += 0.01 * basePremium;
		}
		if(dailyexercise){
			basePremium -= 0.03 * basePremium;
		}
		if(smoking){
			basePremium += 0.03 * basePremium;
		}
		if(alcohol){
			basePremium += 0.03 * basePremium;
		}
		if(drugs){
			basePremium += 0.03 * basePremium;
		}
		
		return basePremium;
	}

	

	
	

}
